import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validators, FormControl, ValidatorFn, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLongitud = 3;
  
  constructor(fb: FormBuilder) { 
	this.onItemAdded = new EventEmitter();
	this.fg = fb.group({
	  nombre: ['', Validators.compose([
            Validators.required,
            this.nombreValidator,
            this.nombreValidatorParametrizable(this.minLongitud)
          ])],
      url: ['']
	});
	
	this.fg.valueChanges.subscribe((form: any) => {
		console.log('cambio en el formulario:', form);
	});
  }

  ngOnInit(): void {
  }
  
  guardar(nombre:string, url:string):boolean{
	  const d = new DestinoViaje(nombre, url);
	  this.onItemAdded.emit(d);
	  return false;  /*return false para q no haga nada automaticamente en el HTML*/
  }
  
  nombreValidator(control: FormControl): {[s: string]: boolean}{
	  const l = control.value.toString().trim().length;	  
	  if (l > 0 && l < 5) {
		  return { invalidNombre: true};
	  }
	  return { invalidNombre: false};
  }
  
  nombreValidatorParametrizable(minLong: number): ValidatorFn {
  return (control: AbstractControl): {[s: string]: boolean} | null => {
	  const l = control.value.toString().trim().length;	  
	  if (l > 0 && l < minLong) {
		  return { minLongNombre: true};
	  }
	  return null;
  }
  }
}
