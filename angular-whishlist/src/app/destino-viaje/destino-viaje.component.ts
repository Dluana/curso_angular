import { Component, OnInit, Input, HostBinding, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
  @Input() destino!: DestinoViaje;	
  @Input('idx') position!: number;	
  @HostBinding('attr.class')cssClass = 'col-md-4';
  @Output() clicked: EventEmitter<DestinoViaje>;	/*se declara la propiedad clicked.  Se pone output porque es un valor de salida */
  
  constructor(){
	  this.clicked = new EventEmitter();  /*inicializamos la propiedad*/
  }
  
  ngOnInit(){
  }

  ir(){
	  this.clicked.emit(this.destino); /*emite evendo al componente padre. da a conocer q destino fue seleccionado*/
	  return false; /*para q no genere efecto en el HTML*/
  }
}
