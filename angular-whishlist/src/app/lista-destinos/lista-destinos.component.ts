import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';
import { DestinosApiClient } from './../models/destinos-api-client.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  /*destinos: DestinoViaje[];*/
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  
  /*constructor() { 
	this.destinos = [];
  }*/
  
  constructor(public destinosApiClient:DestinosApiClient) { 
	this.onItemAdded = new EventEmitter();
  }
	
  ngOnInit(): void {
  }

  /*guardar(nombre:string, url:string):boolean{
	  this.destinos.push(new DestinoViaje(nombre, url));
	  return false;
  } */ 
  agregado(d: DestinoViaje){
	  this.destinosApiClient.add(d);
	  this.onItemAdded.emit(d);	  
  }
  
  elegido(e: DestinoViaje){
	  /*this.destinos.forEach(function (x){x.setSelected(false);});*/
	  this.destinosApiClient.getAll().forEach(x => x.setSelected(false));
	  e.setSelected(true);
  }
}
